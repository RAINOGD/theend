<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Review
 *
 * @ORM\Table(name="review")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ReviewRepository")
 */
class Review
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     * @ORM\Column(name="datetime", type="datetime", unique=false)
     */
    private $datetime;

    /**
     * @var string
     *
     * @ORM\Column(name="commentary", type="text", nullable=true)
     */
    private $commentary;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Building", inversedBy="reviews")
     * @ORM\JoinColumn(name="building_id", referencedColumnName="id")
     */
    private $building;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="owned_reviews")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $author;

    /**
     * @var int
     * @ORM\Column(name="building_quality", type="integer", nullable=true)
     */
    private $building_quality;

    /**
     * @var int
     * @ORM\Column(name="service_quality", type="integer", nullable=true)
     */
    private $service_quality;

    /**
     * @var int
     * @ORM\Column(name="environment", type="integer", nullable=true)
     */
    private $environment;
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set commentary
     *
     * @param string $commentary
     *
     * @return Review
     */
    public function setCommentary($commentary)
    {
        $this->commentary = $commentary;

        return $this;
    }

    /**
     * Get commentary
     *
     * @return string
     */
    public function getCommentary()
    {
        return $this->commentary;
    }

    /**
     * Set building
     *
     * @param \AppBundle\Entity\Building $building
     *
     * @return Review
     */
    public function setBuilding(\AppBundle\Entity\Building $building = null)
    {
        $this->building = $building;

        return $this;
    }

    /**
     * Get building
     *
     * @return \AppBundle\Entity\Building
     */
    public function getBuilding()
    {
        return $this->building;
    }

    /**
     * Set author
     *
     * @param \AppBundle\Entity\User $author
     *
     * @return Review
     */
    public function setAuthor(\AppBundle\Entity\User $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \AppBundle\Entity\User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set buildingQuality
     *
     * @param integer $buildingQuality
     *
     * @return Review
     */
    public function setBuildingQuality($buildingQuality)
    {
        $this->building_quality = $buildingQuality;

        return $this;
    }

    /**
     * Get buildingQuality
     *
     * @return integer
     */
    public function getBuildingQuality()
    {
        return $this->building_quality;
    }

    /**
     * Set serviceQuality
     *
     * @param integer $serviceQuality
     *
     * @return Review
     */
    public function setServiceQuality($serviceQuality)
    {
        $this->service_quality = $serviceQuality;

        return $this;
    }

    /**
     * Get serviceQuality
     *
     * @return integer
     */
    public function getServiceQuality()
    {
        return $this->service_quality;
    }

    /**
     * Set environment
     *
     * @param integer $environment
     *
     * @return Review
     */
    public function setEnvironment($environment)
    {
        $this->environment = $environment;

        return $this;
    }

    /**
     * Get environment
     *
     * @return integer
     */
    public function getEnvironment()
    {
        return $this->environment;
    }

    /**
     * @return \DateTime
     */
    public function getDatetime(): \DateTime
    {
        return $this->datetime;
    }

    /**
     * @param \DateTime $datetime
     */
    public function setDatetime(\DateTime $datetime)
    {
        $this->datetime = $datetime;
    }
}

<?php
namespace AppBundle\Entity;

use AppBundle\Entity\Photo;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Photo", mappedBy="author")
     */
    protected $owned_photos;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Review", mappedBy="author")
     */
    protected $owned_reviews;



    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Building", mappedBy="author")
     */
    protected $owned_buildings;

    public function __construct()
    {
        parent::__construct();
        $this->owned_reviews = new ArrayCollection();
        $this->owned_buildings = new ArrayCollection();
        $this->owned_photos = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function __toString()
    {
        return $this->email ?: '';
    }

    /**
     * Add ownedPhoto
     *
     * @param Photo $ownedPhoto
     *
     * @return User
     */
    public function addOwnedPhoto(Photo $ownedPhoto)
    {
        $this->owned_photos[] = $ownedPhoto;

        return $this;
    }

    /**
     * Remove ownedPhoto
     *
     * @param Photo $ownedPhoto
     */
    public function removeOwnedPhoto(Photo $ownedPhoto)
    {
        $this->owned_photos->removeElement($ownedPhoto);
    }

    /**
     * Get ownedPhotos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOwnedPhotos()
    {
        return $this->owned_photos;
    }

    /**
     * Add ownedBuilding
     *
     * @param \AppBundle\Entity\Building $ownedBuilding
     *
     * @return User
     */
    public function addOwnedBuilding(\AppBundle\Entity\Building $ownedBuilding)
    {
        $this->owned_buildings[] = $ownedBuilding;

        return $this;
    }

    /**
     * Remove ownedBuilding
     *
     * @param \AppBundle\Entity\Building $ownedBuilding
     */
    public function removeOwnedBuilding(\AppBundle\Entity\Building $ownedBuilding)
    {
        $this->owned_buildings->removeElement($ownedBuilding);
    }

    /**
     * Get ownedBuildings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOwnedBuildings()
    {
        return $this->owned_buildings;
    }

    /**
     * Add ownedReview
     *
     * @param \AppBundle\Entity\Review $ownedReview
     *
     * @return User
     */
    public function addOwnedReview(\AppBundle\Entity\Review $ownedReview)
    {
        $this->owned_reviews[] = $ownedReview;

        return $this;
    }

    /**
     * Remove ownedReview
     *
     * @param \AppBundle\Entity\Review $ownedReview
     */
    public function removeOwnedReview(\AppBundle\Entity\Review $ownedReview)
    {
        $this->owned_reviews->removeElement($ownedReview);
    }

    /**
     * Get ownedReviews
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOwnedReviews()
    {
        return $this->owned_reviews;
    }
}

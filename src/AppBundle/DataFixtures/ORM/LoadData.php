<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Building;
use AppBundle\Entity\Category;
use AppBundle\Entity\Photo;
use AppBundle\Entity\Review;
use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

class LoadData implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $encoder = $this->container->get('security.password_encoder');
        $admin = new User();
        $admin->setEmail('original@some.com')
            ->setUsername('rain')
            ->setRoles(['ROLE_ADMIN'])
            ->setEnabled(true);

        $password = $encoder->encodePassword($admin, 'rain');
        $admin->setPassword($password);

        $manager->persist($admin);

        $users = [];
        for ($i = 1; $i <= 4; $i++){
            $user = new User();
            $user->setEmail('email'. $i . '@gmail.com')
                ->setEnabled(true)
                ->setRoles(['ROLE_USER'])
                ->setUsername('User'. $i);

            $password = $encoder->encodePassword($admin, 'user' . $i);
            $user->setPassword($password);
            $manager->persist($user);
            $users[] = $user;
        }



        $categoryNames = ['Cafe', 'Pub', 'Bar', 'Restaurant', 'Tavern'];
        $categories = [];
        foreach ($categoryNames as $categoryName){
            $category = new Category();
            $category->setName($categoryName);

            $manager->persist($category);
            $categories[] = $category;
        }

        $buildingNames = ['Ahunbaev Street Bar', 'Manas Manas Manas', 'Rainy Life', 'Wulfgar', 'Sugar', 'Cesar', 'Rome', 'Wine'];
        $photoNum = 1;
        $buildings = [];
        foreach ($buildingNames as $buildingName){
            $photoNum < 6 ? $active = true : $active = false;
            $building = new Building();
            $building->setIsActive($active);
            $building->setMainPhoto('cafe-'.$photoNum .'.jpg');
            $building->setName($buildingName);
            $building->setDescription('Описание!');
            $building->setCategory($categories[rand(0, count($categoryNames) - 1)]);
            $building->setAuthor($users[rand(0, count($users) - 1)]);

            $manager->persist($building);
            $buildings[] = $building;
            $photoNum++;
        }
        $count = count($users);
        for ($i = 0; $i < $count; $i++){
            $review = new Review();
            $review->setDatetime(new \DateTime());
            $review->setBuilding($buildings[rand(0, count($buildings) - 1)]);
            $review->setAuthor($users[$i]);
            $review->setCommentary('Comment');
            $review->setBuildingQuality(random_int(1, 5));
            $review->setServiceQuality(random_int(1, 5));
            $review->setEnvironment(random_int(1, 5));

            $manager->persist($review);

        }


        for ($i = 1; $i <= 20; $i++){
            $photo = new Photo();
            $photo->setAuthor($users[rand(0, $count - 1)])
                ->setBuilding($buildings[rand(0, count($buildings) - 1)])
                ->setPhotoName('cafe-' . rand(1, 8) . '.jpg');
            $manager->persist($photo);
        }

        $manager->flush();
    }
}
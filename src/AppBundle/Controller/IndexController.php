<?php

namespace AppBundle\Controller;

use AppBundle\Services\StatisticManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class IndexController extends Controller
{
    /**
     * @Route("/{id}", requirements={"id": "\d+"})
     * @Method({"GET", "POST"})
     * @param null $id
     * @param Request $request
     * @param StatisticManager $statisticManager
     * @return \Symfony\Component\HttpFoundation\Response
     * @internal param $category_id
     */
    public function index($id = null, Request $request, StatisticManager $statisticManager)
    {
        $categories = $this->getDoctrine()->getRepository('AppBundle:Category')->findAll();
        $overalls = [];

        if ($request->get('parameter') == null){
            if ($id){
                $buildings = $this->getDoctrine()->getRepository('AppBundle:Building')->getActiveBuildingByCategory($id);
            } else {
                $buildings = $this->getDoctrine()->getRepository('AppBundle:Building')->findBy(['isActive' => true]);
            }
        } else {
            $buildings = $this->getDoctrine()->getRepository('AppBundle:Building')
                ->search($request->get('parameter'));
        }


        foreach ($buildings as $building) {
            $overalls[$building->getId()] = [
                $statisticManager->getOverall($building->getReviews()),
                $statisticManager->getReviewsCount($building->getReviews()),
                $statisticManager->getPhotosCount($building->getBuildingPhotos())
            ];
        }

        /**
         * @var $paginator \Knp\Component\Pager\Paginator
         */
        $paginator = $this->get('knp_paginator');
        $paginate_building = $paginator->paginate(
            $buildings,
            $request->query->getInt('page', 1), 2
        );


        return $this->render('@App/Index/index.html.twig', array(
            'categories' => $categories,
            'paginate' => $paginate_building,
            'overalls' => $overalls
        ));
    }
}

<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Building;
use AppBundle\Entity\Photo;
use AppBundle\Entity\Review;
use AppBundle\Services\StatisticManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class BuildingController extends Controller
{

    /**
     * @Method({"GET", "POST"})
     * @Route("/building/{id}")
     * @param int $id
     * @param StatisticManager $statisticManager
     * @return \Symfony\Component\HttpFoundation\Response
     * @internal param Request $request
     */
    public function showAction(int $id, StatisticManager $statisticManager){
        $building = $this->getDoctrine()->getRepository('AppBundle:Building')->find($id);
        $reviews = $this->getDoctrine()->getRepository('AppBundle:Review')->findBy(['building' => $id]);

        if ($this->getUser()) {
            foreach ($reviews as $review) {
                if ($review->getAuthor()->getId() == $this->getUser()->getId()) {
                    $may_leave_comment = false;
                }
            }
        }

        $data = [
            'overall' => $statisticManager->getOverall($reviews),
            'bq_rating' => $statisticManager->getBuildingOverallRating($reviews),
            'sq_rating' => $statisticManager->getServiceOverallRating($reviews),
            'env_rating' => $statisticManager->getEnvironmentOverallRating($reviews),
        ];


        $form = $this->createForm('AppBundle\Form\ReviewType', null, [
            'method' => 'POST',
            'action' => $this->generateUrl('app_building_review', [
                'building_id' => $id
            ])
        ]);

        $photoForm = $this->createForm('AppBundle\Form\PhotoType', null, [
            'method' => 'POST',
            'action' => $this->generateUrl('app_building_photoupload', [
                'building_id' => $id
            ])
        ]);

        $deleteForm = $this->createForm('AppBundle\Form\DeleteType', null, [
            'method' => 'DELETE'
        ]);

        return $this->render('@App/Building/show.html.twig', [
            'form' => $form->createView(),
            'photoUpload' => $photoForm->createView(),
            'building' => $building,
            'reviews' => $reviews,
            'data' => $data,
            'deleteForm' => $deleteForm->createView(),
            'may_leave_comment' => $may_leave_comment ?? true
        ]);
    }

    /**
     * @Method("DELETE")
     * @Route("/delete/{id}")
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(int $id){
        $review = $this->getDoctrine()
            ->getRepository(Review::class)
            ->find($id);
        if ($this->getUser()->getId() == $review->getAuthor()->getId()){
            $em = $this->getDoctrine()->getManager();
            $building = $review->getBuilding();
            $em->remove($review);
            $em->flush();

            return $this->redirectToRoute('app_building_show', array(
                'id' => $building->getId()
            ));
        }
        return $this->redirectToRoute('fos_user_security_login');
    }

    /**
     * @Method("POST")
     * @Route("/photo/{building_id}")
     * @param int $building_id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function photoUploadAction(int $building_id, Request $request){
        $building = $this->getDoctrine()->getRepository('AppBundle:Building')
            ->find($building_id);

        $photo = new Photo();
        $form = $this->createForm('AppBundle\Form\PhotoType', $photo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $photo->setAuthor($this->getUser());
            $photo->setBuilding($building);
            $em = $this->getDoctrine()->getManager();
            $em->persist($photo);
            $em->flush();


        }
        return $this->redirectToRoute('app_building_show', [
            'id' => $building_id
        ]);

    }


    /**
     * @Method("POST")
     * @Route("/review/{building_id}")
     * @param int $building_id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function reviewAction(int $building_id, Request $request){
        $building = $this->getDoctrine()->getRepository('AppBundle:Building')
            ->find($building_id);

        $review = new Review();
        $form = $this->createForm('AppBundle\Form\ReviewType', $review);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $review->setDatetime(new \DateTime());
            $review->setAuthor($this->getUser());
            $review->setBuilding($building);
            $em = $this->getDoctrine()->getManager();
            $em->persist($review);
            $em->flush();


        }
        return $this->redirectToRoute('app_building_show', [
            'id' => $building_id
        ]);

    }


    /**
     * @Method({"POST", "GET"})
     * @Route("/create")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        $building = new Building();
        $form = $this->createForm('AppBundle\Form\BuildingType', $building);
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()){
            $em = $this->getDoctrine()->getManager();
            $building->setAuthor($this->getUser());
            $building->setIsActive(false);
            $em->persist($building);
            $em->flush();

            return $this->redirectToRoute('app_building_userbuildingslist', [
                'id' => $this->getUser()->getId()
            ]);
        }

        return $this->render('AppBundle:Building:create.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Method("GET")
     * @Route("/buildings-list/{id}")
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction(int $id){
        $category = $this->getDoctrine()->getRepository('AppBundle:Category')
            ->find($id);
        return $this->render('buildings.html.twig', [
            'buildings' => $category->getBuildings()
        ]);
    }

    /**
     * @Method("GET")
     * @Route("/buildings-list/user/{id}")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function userBuildingsList($id){
        $buildings = $this->getDoctrine()->getRepository('AppBundle:Building')
            ->findBy(['author' => $id]);

        return $this->render('@App/Building/user_builds_list.html.twig', [
            'buildings' => $buildings
        ]);
    }

}

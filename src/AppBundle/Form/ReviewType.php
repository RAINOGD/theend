<?php

namespace AppBundle\Form;

use blackknight467\StarRatingBundle\Form\RatingType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReviewType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('commentary', TextareaType::class, [
            'required' => false
        ])
            ->add('building_quality', RatingType::class, [
                'label' => 'Rating'
            ])
            ->add('service_quality', RatingType::class, [
                'label' => 'Rating'
            ])
            ->add('environment', RatingType::class, [
                'label' => 'Rating'
            ])
            ->add('send', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getBlockPrefix()
    {
        return 'app_bundle_review_type';
    }
}

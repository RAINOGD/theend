<?php
/**
 * Created by PhpStorm.
 * User: rain
 * Date: 11/15/17
 * Time: 3:36 PM
 */
namespace AppBundle\Services;


use AppBundle\Entity\Review;

class StatisticManager
{
    public function getOverall($reviews)
    {
        if ($this->getReviewsCount($reviews) == 0){
            return 0;
        }

        $sum = 0;
        /** @var Review $review */
        foreach ($reviews as $review) {
            $sum += ($review->getBuildingQuality()
                + $review->getServiceQuality()
                + $review->getEnvironment());
        }

        $sum = $sum / (count($reviews) * 3);

        return $sum;
    }

    public function getBuildingOverallRating($reviews){
        if ($this->getReviewsCount($reviews) == 0){
            return 0;
        }

        $sum = 0;
        /** @var Review $review */
        foreach ($reviews as $review){
            $sum += $review->getBuildingQuality();
        }

        return $sum / count($reviews);
    }

    public function getServiceOverallRating($reviews){
        if ($this->getReviewsCount($reviews) == 0){
            return 0;
        }

        $sum = 0;
        /** @var Review $review */
        foreach ($reviews as $review){
            $sum += $review->getServiceQuality();
        }

        return $sum / count($reviews);;
    }

    public function getEnvironmentOverallRating($reviews){
        if ($this->getReviewsCount($reviews) == 0){
            return 0;
        }

        $sum = 0;
        /** @var Review $review */
        foreach ($reviews as $review){
            $sum += $review->getEnvironment();
        }

        return $sum / count($reviews);
    }

    public function getReviewsCount($reviews){
        return count($reviews);
    }

    public function getPhotosCount($photos){
        return count($photos);
    }
}